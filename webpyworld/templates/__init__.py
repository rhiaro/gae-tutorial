from web.template import CompiledTemplate, ForLoop, TemplateResult


# coding: utf-8
def index (notes):
    __lineoffset__ = -4
    loop = ForLoop()
    self = TemplateResult(); extend_ = self.extend
    extend_([u'<!doctype html>\n'])
    extend_([u'<html>\n'])
    extend_([u'        <head>\n'])
    extend_([u'                <title>Webpy test</title>\n'])
    extend_([u'        </head>\n'])
    extend_([u'        <body>\n'])
    extend_([u'                <h1>Hello world</h1>\n'])
    extend_([u'                <h2>Last ten notes..</h2>\n'])
    extend_([u'\n'])
    for note in loop.setup(notes):
        extend_(['                ', u'    <p>', escape_(note.content, True), u' <time>(', escape_(note.date, True), u')</time></p>\n'])
        extend_(['                ', u'\n'])
    extend_([u'                <h2>Add a note</h2>\n'])
    extend_([u'                <form method="post" action="/note">\n'])
    extend_([u'                        <p><textarea name="content"></textarea></p>\n'])
    extend_([u'                        <p><input type="submit" value="Save" /></p>\n'])
    extend_([u'\n'])
    extend_([u'        </body>\n'])
    extend_([u'</html>\n'])

    return self

index = CompiledTemplate(index, 'templates\\index.html')
join_ = index._join; escape_ = index._escape

